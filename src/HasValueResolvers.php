<?php

namespace Dluchs\Sieve;

use Dluchs\Sieve\Contracts\Resolver;
use Dluchs\Sieve\Contracts\Sifter;

trait HasValueResolvers
{
    /**
     * The list of the registered value-resolvers.
     *
     * @var \Dluchs\Sieve\Contracts\Resolver[]
     */
    protected static $valueResolvers = [];

    /**
     * Determine if a given class has been added.
     *
     * @param  string  $class
     * @return bool
     */
    public static function hasValueResolver($key)
    {
        return array_key_exists($key, static::$valueResolvers);
    }

    /**
     * Add a value-resolver
     *
     * @param  \Dluchs\Sieve\Contracts\Resolver  $resolver
     * @return void
     */
    public static function addValueResolver(Resolver $resolver)
    {
        static::$valueResolvers[$resolver->getKey()] = $resolver;
    }

    /**
     * Iterate over the resolvers and set value on Sifter
     * 
     * @param \Dluchs\Sieve\Contracts\Sifter  $sifter
     */
    public static function resolveValue(Sifter $sifter): void
    {
        foreach(static::$valueResolvers as $valueResolver) {
            if ($valueResolver->resolves($sifter) && $sifter->resolvesValue($valueResolver)) {
                $resolvedValue = $valueResolver->resolve($sifter);

                if ($sifter->validateResolvedValue($resolvedValue)) {
                    $sifter->resolvedValue($resolvedValue);
                    return;
                }
            }
        }

        return;
    }
}