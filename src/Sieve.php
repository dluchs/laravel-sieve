<?php

namespace Dluchs\Sieve;

use Dluchs\Sieve\Contracts\ResolvableValueSifter;
use Dluchs\Sieve\Contracts\Sifter;
use Dluchs\Sieve\Contracts\Resolver;

class Sieve
{
    use HasSifters;
    use HasValueResolvers;

    public static function add(...$args)
    {
        foreach($args as $object)
        {
            if ($object instanceof Resolver) {
                static::addValueResolver($object);
            }

            if ($object instanceof Sifter) {
                static::addSifter($object);
            }
        }
    }
    
    public static function sift($resource)
    {
        foreach(static::$sifters as $sifter)
        {
            // Resolve if possible
            if (static::isResolveableValueSifter($sifter)) {
                static::resolveValue($sifter);
            }
            
            // sift the resource 
            $resource = ($result = $sifter->sift($resource)) ? $result : $resource;
        }
        
        return $resource;
    }

    public static function isResolveableValueSifter(Sifter $sifter)
    {
        return is_subclass_of($sifter, 'Dluchs\Sieve\Contracts\ResolvableValueSifter');
    }
}