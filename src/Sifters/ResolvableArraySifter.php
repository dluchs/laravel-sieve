<?php

namespace Dluchs\Sieve\Sifters;

use Dluchs\Sieve\Contracts\ResolvableValueSifter;

class ResolvableArraySifter extends ArraySifter implements ResolvableValueSifter
{
    protected $hasResolvedValue = false;

    public function __construct($key)
    {
        $this->key = $key;
    }

    public function sift($array)
    {
        if ($this->hasResolvedValue) {
            return parent::sift($array);
        }
    }

    public function resolvesValue($valueResolver): bool
    {
        return is_null($this->value);
    }

    /**
     * Callback for the value resolved by a Resolver.
     *
     * @param  mixed  $resolvedValue
     * @return void
     */
    public function resolvedValue($resolvedValue)
    {
        $this->value = $resolvedValue;

        $this->hasResolvedValue = true;
    }

    public function resolveValueKey(): string
    {
        return $this->getKey();
    }

    /**
     * Return if the resolved value is valid
     * 
     * @return bool 
     */
    public function validateResolvedValue($resolvedValue): bool
    {
        return !!$resolvedValue;
    }
}