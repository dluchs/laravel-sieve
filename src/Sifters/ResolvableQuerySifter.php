<?php

namespace Dluchs\Sieve\Sifters;

use Dluchs\Sieve\Contracts\ResolvableValueSifter;

class ResolvableQuerySifter extends QuerySifter implements ResolvableValueSifter
{
    protected $hasResolvedValue = false;
    
    protected $resolveValueKey;

    public function __construct($resolveValueKey, $column)
    {
        $this->resolveValueKey = $resolveValueKey;
        $this->column = $column;
    }

    /**
     * Sift the Query
     * 
     * @param  \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder  $builder
     */
    public function sift($builder)
    {
        if ($this->hasResolvedValue) {
            parent::sift($builder);
        }
    }
    
    public function resolvesValue($valueResolver): bool
    {
        return $this->hasResolvedValue == false;
    }

    /**
     * Callback for the value resolved by a Resolver.
     *
     * @param  mixed  $resolvedValue
     * @return void
     */
    public function resolvedValue($resolvedValue)
    {
        $this->value = $resolvedValue;

        $this->hasResolvedValue = true;
    }

    public function resolveValueKey(): string
    {
        return $this->resolveValueKey;
    }

    /**
     * Return if the resolved value is valid
     * 
     * @return bool 
     */
    public function validateResolvedValue($resolvedValue): bool
    {
        return !!$resolvedValue;
    }
}