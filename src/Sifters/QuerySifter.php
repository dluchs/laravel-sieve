<?php

namespace Dluchs\Sieve\Sifters;

use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

use Dluchs\Sieve\Contracts\Sifter;
class QuerySifter extends AbstractSifter implements Sifter
{
    protected $column;

    public function __construct($column, $value)
    {
        $this->column = $column;
        $this->value = $value;
    }
    
    /**
     * Sift the Query
     * 
     * @param  \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder  $builder
     */
    public function sift($builder)
    {
        call_user_func($this->comparator(), $builder);
    }

    public function sifts($builder): bool
    {
        return $builder instanceof QueryBuilder || $builder instanceof EloquentBuilder;
    }

    public function comparator()
    {
        if (is_array($this->value)) {
            return [$this, 'whereInComparator'];
        } else {
            return [$this, 'whereComparator'];
        }
    }
    
    public function whereInComparator($builder)
    {
        $builder->whereIn($this->column, $this->value);
    }

    public function whereComparator($builder)
    {
        $builder->where($this->column, '=', $this->value);
    }

    /**
     * Get the value of column
     */ 
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * Set the value of column
     *
     * @return  self
     */ 
    public function setColumn($column)
    {
        $this->column = $column;

        return $this;
    }
}