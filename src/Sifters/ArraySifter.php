<?php

namespace Dluchs\Sieve\Sifters;

use Illuminate\Support\Arr;

use Dluchs\Sieve\Contracts\Sifter;

class ArraySifter extends AbstractSifter implements Sifter
{
    public function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }
    
    public function sift($array)
    {
        return Arr::where($array, $this->comparator());
    }

    public function sifts($resource): bool
    {
        return is_array($resource);
    }
    
    public function comparator()
    {
        if (is_array($this->value)) {
            return [$this, 'inArrayComparator'];
        } else {
            return [$this, 'matchComparator'];
        }
    }
    
    public function inArrayComparator($item)
    {
        return Arr::has($item, $this->key) ? Arr::get($item[$this->key], $this->value, false) : false;
    }

    public function matchComparator($item)
    {
        return $item[$this->key] == $this->value;
    }

    public function exactMatchComparator($item)
    {
        return $item[$this->key] === $this->value;
    }
}