<?php

namespace Dluchs\Sieve\Resolvers;

class AbstractValueResolver
{
    protected $key;
    
    /**
     * Get the value of key
     */ 
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set the value of key
     *
     * @return  self
     */ 
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }
}