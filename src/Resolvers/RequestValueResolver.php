<?php

namespace Dluchs\Sieve\Resolvers;

use Dluchs\Sieve\Sieve;
use Dluchs\Sieve\Contracts\Resolver;

use Illuminate\Http\Request;

class RequestValueResolver extends AbstractValueResolver implements Resolver
{
    protected $request;

    protected $baseValueKey;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function resolves($sifter): bool
    {
        return Sieve::isResolveableValueSifter($sifter);
    }

    public function resolve($sifter)
    {
        return $this->resolveKey($this->baseValueKey . $sifter->resolveValueKey());
    }

    public function resolveKey($key)
    {
        return $this->request->input($key);
    }

    /**
     * Get the value of key
     */
    public function getBaseValueKey()
    {
        return $this->baseValueKey;
    }

    /**
     * Set the value of key
     *
     * @return  self
     */
    public function setBaseValueKey($baseValueKey)
    {
        $this->baseValueKey = $baseValueKey;

        return $this;
    }
}
