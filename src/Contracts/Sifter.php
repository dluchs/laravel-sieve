<?php

namespace Dluchs\Sieve\Contracts;

interface Sifter
{
    /**
     * Sift the given resource.
     *
     * @param  mixed  $resource
     * @return void
     */
    public function sift($resource);

    /**
     * Return if the Sifter will sift the given resource.
     *
     * @param  mixed  $resource
     * @return void
     */
    public function sifts($resource): bool;
}