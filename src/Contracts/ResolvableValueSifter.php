<?php

namespace Dluchs\Sieve\Contracts;

use Dluchs\Sieve\Contracts\Resolver;

interface ResolvableValueSifter
{
    /**
     * Callback for the value resolved by a Resolver.
     *
     * @param  mixed  $resolvedValue
     * @return void
     */
    public function resolvedValue($resolvedValue);

    /**
     * Return if the Sifter wants to have the value resolved
     *
     * @param  mixed  $resource
     * @return bool
     */
    public function resolvesValue(Resolver $resolver): bool;

    /**
     * Return the key used by the Resolver
     * 
     * @return string 
     */
    public function resolveValueKey(): string;

    /**
     * Return if the resolved value is valid
     * 
     * @return bool 
     */
    public function validateResolvedValue($resolvedValue): bool;
}