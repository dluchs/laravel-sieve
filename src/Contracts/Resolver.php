<?php

namespace Dluchs\Sieve\Contracts;

interface Resolver
{
    /**
     * Resolve the value for a given Sifter.
     *
     * @param  mixed  $resource
     * @return void
     */
    public function resolve(Sifter $sifter);

    /**
     * Return if the Resolver will resolve the given Sifter object.
     *
     * @param  mixed  $resource
     * @return void
     */
    public function resolves(Sifter $sifter): bool;
}