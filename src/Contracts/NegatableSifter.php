<?php

namespace Dluchs\Sieve\Contracts;

interface NegatableSifter
{
    /**
     * Callback to negate the sifter.
     *
     * @param  mixed  $resolvedValue
     * @return void
     */
    public function negate();
}