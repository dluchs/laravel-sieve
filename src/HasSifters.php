<?php

namespace Dluchs\Sieve;

use Dluchs\Sieve\Contracts\Sifter;

trait HasSifters
{
    /**
     * The list of the registered sifters.
     *
     * @var \Dluchs\Sieve\Contracts\Sifter[]
     */
    protected static $sifters = [];

    /**
     * Determine if a given class has been added.
     *
     * @param  string  $key
     * @return bool
     */
    public static function hasSifter($key)
    {
        return array_key_exists($key, static::$sifters);
    }

    /**
     * get an existing sifter.
     *
     * @param  string  $$key
     * @return \Dluchs\Sieve\Contracts\Sifter
     */
    public static function getSifter($key)
    {
        return data_get(static::$sifters, $key);
    }

    /**
     * Add a Sifters
     *
     * @param  \Dluchs\Sieve\Contracts\Sifter  $sifter
     * @return void
     */
    public static function addSifter(Sifter $sifter)
    {
        static::$sifters[$sifter->getKey()] = $sifter;
    }
}