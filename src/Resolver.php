<?php

namespace Dluchs\Sieve;

use Dluchs\Sieve\Contracts\Resolver as ResolverContract;
use Dluchs\Sieve\Resolvers\RequestValueResolver;

class Resolver
{
    public static function requestValue($key, $baseValueKeyOrClassOrObject)
    {
        if ($baseValueKeyOrClassOrObject instanceof ResolverContract) {
            $resolver = $baseValueKeyOrClassOrObject;
        } 
        elseif (class_exists($baseValueKeyOrClassOrObject)) {
            $resolver = resolve($baseValueKeyOrClassOrObject);
        } 
        else {
            $resolver = resolve(RequestValueResolver::class)->setBaseValueKey($baseValueKeyOrClassOrObject);
        }

        return tap($resolver)->setKey($key);
    }
}