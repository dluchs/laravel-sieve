<?php

namespace Dluchs\Sieve;

use Dluchs\Sieve\Contracts\Sifter as SifterContract;

use Dluchs\Sieve\Sifters\ArraySifter;
use Dluchs\Sieve\Sifters\QuerySifter;
use Dluchs\Sieve\Sifters\ResolvableArraySifter;
use Dluchs\Sieve\Sifters\ResolvableQuerySifter;

class Sifter
{
    public static function array($key, $valueOrClassOrObject = null)
    {
        return static::create($key, $valueOrClassOrObject, $valueOrClassOrObject == null ? ResolvableArraySifter::class : ArraySifter::class);
    }

    public static function query($key, $valueOrClassOrObject = null)
    {
        return static::create($key, $valueOrClassOrObject, $valueOrClassOrObject == null ? ResolvableQuerySifter::class : QuerySifter::class);
    }

    public static function resolveArray($key, $resolveValueKeyOrClassOrObject)
    {
        return static::create($key, $resolveValueKeyOrClassOrObject, ResolvableArraySifter::class);
    }

    public static function resolveQuery($key, $resolveValueKeyOrClassOrObject, $column = null)
    {
        return static::create($key, $resolveValueKeyOrClassOrObject, ResolvableQuerySifter::class, [
            'resolveValueKey' => $resolveValueKeyOrClassOrObject,
            'column' => $column
        ]);
    }

    public static function create($key, $valueOrClassOrObject = null, $sifterClass = null, $params = [])
    {
        if ($valueOrClassOrObject instanceof SifterContract) {
            $sifter = $valueOrClassOrObject;
        } 
        elseif (class_exists($valueOrClassOrObject)) {
            $sifter = resolve($valueOrClassOrObject, $params);
        } 
        elseif ($sifterClass != null) {
            $sifter = resolve($sifterClass, $params);
        }
            
        return tap($sifter)->setKey($key);
    }
}